import TiFFILE
import Parser
import os
import ntpath

class MesFile(TiFFILE.TIFFILE):
    """Description: Mes tif image derived class from TIFFILE.

        **Data Members:
                    path:         String, tif image location.
                    directory:    String, tif image folder.
                    file_name:    String, tif metadata name.
                    metadataPath: String, tif metadata location.
                    metadata:     String, tif metadata object.
                    Shape:        String, tif shape.
        **Functions:
                    loadframes:    shared between the children,load the tif image through the input_output.py
                    loadMetaData:  by the help from Parser class.
                    shape:         Extract the value from metadata.
                    width_um:      Extract the value from metadata.
                    height_um:     Extract the value from metadata.
                    image_rect:    Extract the value from metadata.
                    pos_to_um:     Extract the value from metadata.
                    z:             Extract the value from metadata.
                    minZ:          Extract the value from metadata.
                    print:         Extract the value from metadata.
                    save:          shared between the children,save the tif image through the input_output.py

    """

    def __init__(self):
        super().__init__()

    def loadframes(self,engine, path,recursive,number_of_partitions):
        super().loadframes(engine, path, recursive, number_of_partitions)
        self.path = path

    def loadMetaData(self):
        """Description: load the metadata information via Parser.

            **Input:
                    self.path: String, tif image location.
            **Output:
                    self.directory.
                    self.file_name.
                    self.metadataPath.
                    self.metadata.

        """
        super().loadMetaData()

        # extract the folder path
        self.directory = os.path.dirname(self.path)

        # generate the metadata file name.
        self.file_name = ntpath.basename(self.path).split('_')[0] + "_metadata.txt"

        # generate the metadata path.
        self.metadataPath = os.path.join(self.directory, self.file_name)

        # check if the metadata is exist then load it.
        if (os.path.isfile(self.metadataPath)):
            print("Loading Metadata File!")

            # load the information.
            self.metadata = Parser.FemtonicsTifMeta(self.metadataPath)
            # self.metadata = Parser.FemtonicsTifMeta.Mes_Metadata(self.metadataPath)
        else:
            print("The tif Image does not has a metadata file!")

    def shape(self):
        super().shape(self)
        self.Shape = self.metadata.Height, self.metadata.Width
        return self.Shape

    def image_rect(self):
        """image pos, image size for QT"""
        super().image_rect()
        return (self.metadata.WidthOrigin, -(self.metadata.height_um + self.metadata.HeightOrigin), self.metadata.width_um,
                self.metadata.height_um)

    def height_um(self):
        super().height_um()
        return self.metadata.HeightStep * self.metadata.Height

    def width_um(self):
        super().width_um()
        return self.metadata.WidthStep * self.metadata.Width

    def z(self):
        """Get Z from origin of slice#, using python/zero based index"""
        super().z()
        assert 0 <= slice < self.metadata.D3Size
        return (self.metadata.D3Origin - self.metadata.ZlevelArm) + slice * self.metadata.D3Step

    def minZ(self):
        super().minZ()
        return self.metadata.D3Origin, self.metadata.D3Size, self.metadata.D3Step

    def pos_to_um(self, pos):
        super().pos_to_um()
        x =  self.metadata.WidthOrigin  + pos[0] * self.metadata.WidthStep
        y = -self.metadata.HeightOrigin + pos[1] * self.metadata.HeightStep  # QT/tif coords are top left origin, femtonics is bottom left (math)
        return (x, y)

    def print(self,obada):
        """Save the data object to the output."""
        print(obada)

    def save(self):
        super().save(self.file_name+'_corrected.tif')

    def __getattribute__(self, *args, **kwargs):
        return super().__getattribute__(*args, **kwargs)


