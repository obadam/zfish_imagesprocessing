import numpy as np
import thunder as td
from registration import CrossCorr

def Thomas_Initialization(frames,base,nframes=20):
    first_frames = frames[0:nframes, :, :].map(lambda x: base)
    rest = frames[nframes:, :, :]
    frames = td.images.fromrdd(first_frames.tordd().union(rest.tordd()))
    return frames

def base_percentile(frames,nframes=20,percentile=20):
    base = np.percentile(frames[0:nframes, :, :], percentile, 0).astype(frames.dtype)
    return base

def base_sum(nframes,frames):
    base = frames[0:nframes].sum()
    return base

def correction(frames,ref):
    algorithm = CrossCorr()
    reference = ref.mean().toarray()
    model = algorithm.fit(frames, reference=reference)

    return model.transform(frames)