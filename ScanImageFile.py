
import TiFFILE

class ScanImageFile(TiFFILE.TIFFILE):
    """Description: Scan tif image derived class from TIFFILE.

        **Data Members:
                    path:         String, tif image location.
                    directory:    String, tif image folder.
                    metadata:     String, tif metadata object.
                    Shape:        String, tif shape.
        **Functions:
                    loadframes:    shared between the children,load the tif image through the input_output.py
                    loadMetaData:  by the help from Parser class.
                    shape:         Extract the value from metadata.
                    width_um:      Extract the value from metadata.
                    height_um:     Extract the value from metadata.
                    image_rect:    Extract the value from metadata.
                    pos_to_um:     Extract the value from metadata.
                    z:             Extract the value from metadata.
                    minZ:          Extract the value from metadata.
                    print:         Extract the value from metadata.
                    save:          shared between the children,save the tif image through the input_output.py

    """

    def __init__(self):
        super().__init__()

    def loadMetaData(self):
        super().loadMetaData()

    def loadframes(self,engine = None,recursive = False,number_of_partitions = 10):
        super().loadframes(self,engine, self.path, recursive, number_of_partitions)

    def shape(self):
        super().shape()
        self.Shape = self.frames.shape

    def image_rect(self):
        super().image_rect()

    def height_um(self):
        super().height_um()

    def width_um(self):
        super().width_um()

    def z(self):
        super().z()

    def minZ(self):
        super().minZ()

    def pos_to_um(self, pos):
        super().pos_to_um(pos)

    def print(self, p):
        super().print(p)

    def save(self):
        super().save(self.file_name+'_corrected.tif')