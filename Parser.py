import time

class FemtonicsTifMeta(object):
    """
    Parses metadata from Femtonics .txt tif associated files
    """

    def __init__(self, filepath, singlestackformat=False):

        # Parse the Mes Metadata file and store them in a dictionary
        self.Mes_Metadata(filepath)

        # assign the right type for each parameter.
        self.convert_types()

        # N/A
        self.singlestackformat = singlestackformat

        # N/A
        if self.singlestackformat:
            self.fixsinglestack()


    def Mes_Metadata(self,path):
        """Description: Create a dictionary for the metadata attributes of a Mes tiff images.
                        Iterate all lines after decode each one with ISO format. Skip the headlines which start with '<','<<','>','>>','%'
                        and the empty lines. Time attribute is processed solely in addition to directory attribute. all the attributes will
                        be stored as String in the dictionary.

            **Input**
                    path = Metadata file (.txt).
            **Output**
                    self.data_dict = a dictionary has all the mentioned attributes in the given metadata file with their values.
        """

        # open the file with read permission in binary form.
        with open(path, 'rb') as metatxt:

            # read all the lines then store them in 'lines'.
            lines = metatxt.readlines()

            # define the dictionary variable.
            self.data_dict = {}

            # Iterate all lines.
            for line in lines:

                # decode each line to be readable as String.
                line = line.decode("ISO-8859-1")

                # check if the line is empty ( strip = to get rid from the spaces after and before ).
                if not line.strip():
                    continue

                # ignore headlines ( line[0] returns first character of the line ).
                if((line[0].strip() == '%')|(line[0] == '>')|(line[0] == '>>')|(line[0] == '<<')|(line[0] == '<')):
                    continue

                # Time process ( line.split returns a list of words so line.split(':')[0] return the first word before the ':' ).
                if line.split(':')[0].strip() == 'MeasurementDate':
                    self.data_dict['MeasurementDate'] = time.strptime(line[line.find(':') + 1:line.find(',')].strip(),
                                                                      "%Y.%m.%d. %X")
                    continue

                # Path process
                if line.split(':')[0].strip() == 'FileName':
                    self.data_dict['FileName'] = line[line.find(':') + 1:].strip()
                    continue

                # add the rest parameters
                self.data_dict[line.split(':')[0].strip()] = line.split(':')[1].strip()

    def fixsinglestack(self):
        """
        Fixes the metadata from femtonics, - if you export a single tif, the format is different.
        """
        self.data_dict['D3Size'] = 1
        self.data_dict['D3Step'] = 2.5
        self.data_dict['D3Origin'] = self.ZlevelArm

    def check_valid(self, tif):
        assert tif.shape[2] == self.D3Size
        assert tif.shape[0] == self.Height
        assert tif.shape[1] == self.Width

    def convert_types(self):
        """Description: Convert the metadata attributes from String to its real type.

            **Input**
                        self.data_dict
            **Output**
                        self.data_dict
        """

        # Iterate each attribute.
        for key in self.data_dict.keys():

            # check if the current attribute is one of the following as they are already processed.
            if key in ["MeasurementDate", "FileName"]:
                continue

            # Try the int type first.
            try:
                self.data_dict[key] = int(self.data_dict[key])

            # if an error occurs then we try the Float one.
            except ValueError:
                try:
                    self.data_dict[key] = float(self.data_dict[key])

                # if an error occurs again then it will be kept as String.
                except ValueError:
                    pass

    def __str__(self):
        return '\n'.join([str(i) + " " + str(j).strip() + " " + str(type(j)) for i, j in self.data_dict.items()])
        # TODO could print in order

    def __getattr__(self, item):
        return self.data_dict[item]

    @classmethod
    def fromtifpath(cls, tifpath):
        return cls(tifpath.split('_')[0] + "metadata.txt")  # TODO test
