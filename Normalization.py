
def f_Df(frames,mean,max,min):
    """Description: Normalize the passed tif image with f/df way .

        **Input:
                    frames: the tif  image.
                    mean:   the mean image.
                    min:    the min  image.
                    max:    the max  image.
        **Output:
                    return the normalized tif image ( frames ).
    """

    # mean, max and min should be converted to array.
    min  = min.toarray()  #frames.min().toarray()
    max  = max.toarray()  #frames.max().toarray()
    mean = mean.toarray() #frames.mean().toarray().astype(frames.dtype)

    return frames.map(lambda x:((x - mean) / ( max - min ))).astype(frames.dtype)

def series_Normalization(frames,method='percentile', window=None, perc=40, offset=0.1):
    """Description: Normalize the tif image as a series. Normalize by subtracting and dividing by a baseline.
                    Baseline can be derived from a global mean or percentile,or a smoothed percentile estimated within a rolling window.
                    Windowed baselines may only be well-defined for temporal series data.

        **Input:
                    frames:  the tif image.

                    method(baseline):  str, optional, default = 'percentile'
                                       Quantity to use as the baseline, options are 'mean', 'percentile','window', or 'window-exact'.

                    window:  int, optional, default = 6
                             Size of window for baseline estimation,for 'window' and 'window-exact' baseline only.

                    perc:    int, optional, default = 20
                             Percentile value to use, for 'percentile','window', or 'window-exact' baseline only.

                    offset:  float, optional, default = 0.1
                             Scalar added to baseline during division to avoid division by 0.
    """
    # Convert the tig image to a series then normalize it.
    series = frames.toseries().normalize(method, window, perc, offset).astype(frames.dtype)

    # Rr-convert it to a image after the normalization.
    return series.toimages()

