
#region imports

import os
from skimage.io import imread
import thunder as td
import tifffile as tiff
import numpy as np

#endregion

#region load

def load_Thunder(sc=None,path="",file=False,partitions=1):
    """ Description: Load the tif image as list of frames in Thunder way.
                     The load could be with Spark assistance or alone.

        **Input:
                     sc:         Spark Engine, could be None.
                     path:       String,       Image location.
                     file:       bool,         False if its only one tif image, True if it is multiple tif images.
                     partitions: int,          split the tif in 'n' partitions to load in parallel.
        **Output:
                     return the tif image.


    """

    frames = td.images.fromtif(path,'tif',None,None,file,1,partitions,None,sc,None)
    return frames

def load(path):
    """Description: Load the tif images in a given path as --- in the normal way.

        **Input:
                    path: String, Image location.
        **Output:
                    return the tif image.
    """

    # Define the tif image variable.
    images = []

    # extract all the tif images in the given path.
    files = os.listdir(path)

    # Iterate each one.
    for file in files:

        # filter un tiff files.
        if file.endswith('.tiff')|file.endswith('.tif'):

            # generate the path of the current tif image.
            file = path + '/' + file

            # load the image via imread with a spacial plugin for tif images.
            images.append( imread(file,plugin="tifffile"))

    return images

#endregion

#region save

def save(data,path):
    """Description: Save the tif image via the normal way.

        **Input:
                    path: String, location.
                    data: tif image.
        **Output:
                    Nothing to return.
    """

    # The tif image should be as array to be stored.
    data = data.toarray()

    # save the image.
    tiff.imsave(path, data)

def save_Thunder(data,path):
    # not working yet because thunder just support 2d images till now.
    data = data.reduce(lambda x, y: np.dstack((x, y)))
    data.totif('cor/','corr.tif',True)

#endregion

#region Utilities

def timeOverview(times):
    """Description: Calculate the consuming time for each given step.

        **Input:
                    times: list of times ( float ), each values relates to one step.
        **Output:
                    percentages: list of percentages figures.
    """

    # sum all the time variable.
    all = sum(times)

    # define the output list
    percentages = []

    # iterate each one step time and find its percentage from the whole steps.
    for i in range(0,len(times)):
        percentages.append(float(100 * times[i] / float(all)))

    return percentages

#endregion
