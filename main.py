
#region imports

import os
os.environ["PYSPARK_PYTHON"] = "/usr/anaconda3/bin/python3.5"

import input_output
import time

#endregion

#region variables

# tif location on disk
tif_location = '/home/l-01-314/Desktop/mes/F2_pmtUG.tiff'

# tif saved name
tif_save     = 'exmCorrected.tif'

# 10 max
number_of_partitions = 10

# each dim will be divided by it.
sub_sample_factor    = 2

#
percentile           = 20

# normally = 100
nframes = 20

# record the time

times = []

#endregion

#region Spark

# Define Spark Engine.
from pyspark import SparkContext
sc = SparkContext("local[*]", "z-fish")

print('1')

#endregion

#region Tif Image

#Define tif Image Object
start = time.time()
import MesFile

#define the object
TifImage = MesFile.MesFile()
TifImage.loadframes(sc,tif_location,False,number_of_partitions)
TifImage.loadMetaData()
times.append(float(time.time() - start))

# endregion

#region pre-processing

# sub sample the tif image
start = time.time()
TifImage.frames = TifImage.frames.subsample(sub_sample_factor)
times.append(float(time.time() - start))

print('2')

# Normalization
start = time.time()
import Normalization as nm
# TifImage.frames = nm.series_Normalization(TifImage.frames,method='percentile',window=None,perc=40,offset=0.1)
mean = TifImage.frames.mean()
min  = TifImage.frames.min()
max  = TifImage.frames.max()
TifImage.frames = nm.f_Df(TifImage.frames, mean,max,min)
times.append(float(time.time() - start))

print('3')

# filter
start = time.time()
TifImage.frames = TifImage.frames.median_filter(10)
times.append(float(time.time() - start))

print('4')

# Subtract the min image
start = time.time()
min = TifImage.frames.min().toarray()
TifImage.frames = TifImage.frames.subtract(min)
times.append(float(time.time() - start))

print('5')

#endregion

#region Motion Correction

start = time.time()
import MotionCorrection as mc
# ----------Thomas Approach-----------
base = mc.base_percentile(TifImage.frames,nframes,percentile)
TifImage.frames = mc.Thomas_Initialization(TifImage.frames,base,nframes)
ref = TifImage.frames[0:nframes,:,:]
frames_corrected = mc.correction(TifImage.frames,ref)
times.append(float(time.time() - start))

print('6')

#endregion

#region save result

start = time.time()
TifImage.save()
times.append(float(time.time() - start))

print('7')

#endregion

#region Time Overview

percentages = input_output.timeOverview(times)
for i in range(0,len(percentages)):
    print("step %d        time =  %d sec %d %%" % (i,times[i], percentages[i]))

#endregion

