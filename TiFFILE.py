import abc
import input_output

class TIFFILE(metaclass=abc.ABCMeta):
    """Description: Tiffile abstract class holds the frames and their related information.

        **Data Members:
                    frames:
        **Functions:
                    loadframes:    shared between the children,load the tif image through the input_output.py
                    loadMetaData:  each derived class has different implementation.
                    shape:         each derived class has different implementation.
                    width_um:      each derived class has different implementation.
                    height_um:     each derived class has different implementation.
                    image_rect:    each derived class has different implementation.
                    pos_to_um:     each derived class has different implementation.
                    z:             each derived class has different implementation.
                    minZ:          each derived class has different implementation.
                    print:         each derived class has different implementation.
                    save:          shared between the children,save the tif image through the input_output.py

    """

    def __init__(self):
        super().__init__()

    @abc.abstractmethod
    def loadframes(self,engine, path, recursive, number_of_partitions):
        """ Description: Load the tif image as list of frames in Thunder way.
            The load could be with Spark assistance or alone.

                **Input:
                        engine:               Spark Engine, could be None.
                        path:                 String,       Image location.
                        recursive:            bool,         False if its only one tif image, True if it is multiple tif images.
                        number_of_partitions: int,          split the tif in 'n' partitions to load in parallel.
                **Output:
                        self.frames


        """
        self.frames = input_output.load_Thunder(engine, path, recursive, number_of_partitions)

    @abc.abstractmethod
    def loadMetaData(self):
        """Retrieve meta data from the input source
        and return a dictionary.
        """

    @abc.abstractmethod
    def shape(self):
        """Find the shape values."""

    @abc.abstractmethod
    def width_um(self):
        """Find the width."""

    @abc.abstractmethod
    def height_um(self):
        """Find the height."""

    @abc.abstractmethod
    def image_rect(self):
        """Find the rectangle."""

    @abc.abstractmethod
    def pos_to_um(self, pos):
        """."""

    @abc.abstractmethod
    def z(self):
        """."""

    @abc.abstractmethod
    def minZ(self):
        """."""

    @abc.abstractmethod
    def print(self,p):
        """print information."""

    @abc.abstractmethod
    def save(self,name):
        """Description: Save the tif image via the normal way.
            **Input:
                    path: String, location.
                    data: tif image.
            **Output:
                    Nothing to return.
        """
        input_output.save(self.frames, name)


# Register the abstract class.
TIFFILE.register(tuple)

assert issubclass(tuple, TIFFILE)
assert isinstance((), TIFFILE)


